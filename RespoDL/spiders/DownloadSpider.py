# Author: 陈闻超
# Version: 1.0

import scrapy
import codecs
import os

class DownloadSpider(scrapy.Spider):

    def write_file(self, filename, source):
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with codecs.open(filename, 'wb', 'utf-8') as f:
            f.write(source)