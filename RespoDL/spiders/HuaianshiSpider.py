# Author: 陈闻超
# Version: 1.0

import re

from scrapy import Request
from scrapy import FormRequest

from RespoDL.spiders.DownloadSpider import DownloadSpider

class HuaianshiSpider(DownloadSpider):
    name = 'huaianshi'
    start_urls = ['http://222.184.118.98/haportal/jsp/portal/allportalPublicityList.do']

    def build_page_request(self, page):
        return FormRequest("http://222.184.118.98/haportal/jsp/portal/allportalPublicityList.do",
                                  formdata={'formOrigin': '0',
                                            'page': str(page)},
                                  headers={
                                      'referer': 'http://222.184.118.98/haportal/jsp/portal/allportalPublicityList.do'},
                                  callback=self.parse,)

    def parse(self, response):
        cur_page = response.css("div.mainbody table tr td span::text").extract_first().strip()
        filename = 'downloads/' + self.name + '/page_' + cur_page + '/' + cur_page
        self.write_file(filename, response.text)

        rows = response.css("div.list tr")[1:]
        for row in rows:
            cells = row.css("td")
            url = cells.css("a p::attr('onclick')").extract_first()
            url = re.search('\d+', url).group(0)
            url = response.urljoin('allportalPublicityView.do?formId=' + url)
            yield Request(url, meta={'page': cur_page}, callback=self.parse_detail)

        last_page = response.css("div.mainbody table tr td span::text").extract()[1].strip()
        if int(cur_page) < int(last_page):
            yield self.build_page_request(int(cur_page)+1)

    def parse_detail(self, response):
        page = response.request.meta['page']
        page_source = response.text
        page_source = re.sub(r'charset=gb2312\"', 'charset=\"gb2312\"', page_source)
        filename = 'downloads/' + self.name + '/page_' + page + '/' + response.url.split("?")[-1]
        self.write_file(filename, page_source)